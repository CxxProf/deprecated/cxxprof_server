from conans import ConanFile, CMake

class CxxProfConan(ConanFile):
    name = "cxxprof_server"
    version = "1.0.0"
    url = "https://gitlab.com/groups/CxxProf"
    license = "GNU LESSER GENERAL PUBLIC LICENSE Version 3"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    default_options = "ZMQ:shared=True"
    exports = "*"

    def requirements(self):
        self.requires.add("Pluma/1.1@monsdar/testing")
        self.requires.add("Boost/1.59.0@lasote/stable")
        self.requires.add("ZMQ/4.1.1@memsharded/stable")
        self.requires.add("zmqcpp/4.1.1@memsharded/testing")
        self.requires.add("SQLite3/3.12.1@monsdar/testing")
        self.requires.add("SQLite3cc/0.1.1@monsdar/testing")
        self.requires.add("cxxprof_dyn_network/1.1.0@monsdar/testing")
        
    def build(self):
        cmake = CMake(self.settings)
        self.run('cmake %s %s' % (self.conanfile_directory, cmake.command_line))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.exe", dst="bin", src="bin")
